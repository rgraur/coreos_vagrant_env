# CoreOS & Vagrant web environment #

### Requirements ###
* see [CoreOS Vagrant](https://github.com/coreos/coreos-vagrant) dependencies section

### Install ###
* clone repo `git clone git@bitbucket.org:rgraur/coreos_vagrant_env.git vm && cd vm`
* update all submodules (CoreOS vagrant configs and Docker containers) `git submodule update --init --recursive`
* add vagrant configs `ln -s ../config.rb ../user-data vagrant/`
* initialize project `cd vagrant && vagrant up`

### Run ###
* open in browser [http://localhost:8080](http://localhost:8080)
* database access: see [https://bitbucket.org/rgraur/docker_mysql](https://bitbucket.org/rgraur/docker_mysql/)

### Structure ###
* [CoreOS host VM](https://coreos.com) (forwarded ports: 8080:80, 3306:3306; IP: 172.17.8.101)
* [Nginx Docker container](https://bitbucket.org/rgraur/docker_nginx) (forwarded port: 80)
* [MySQL Docker container](https://bitbucket.org/rgraur/docker_mysql) (forwarded port: 3306)
